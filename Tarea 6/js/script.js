function DiagCalc(sides,opt)
{
    var sidesInt = parseInt(sides.value);
    opt.value = sidesInt*(sidesInt-3)/2;
}

function NatNumCalc(objN,opt)
{
    var n = parseInt(objN.value);
    opt.value = n*(n+1)/2;
}

function AngCalc(objN,opt)
{
    var n = parseInt(objN.value);
    opt.value = (n-2)*180/n;
}