package com.fileutil;
 
public class FileInfo {
 
    public static String getFileExtension(String filename) {
        String extension = "";
 
        try {
            if (filename != null) 
            {
                extension = filename.substring(filename.lastIndexOf("."));
            }
        } catch (Exception e) {
            extension = "";
        }
 
        return extension;
    }

    /**
     * Getting the filename from a path
     */
    public static String getFileName(String path) {
        String filename = "";
 
        try {
            if (path != null) 
            {
                filename = path.substring(path.lastIndexOf("/")+1);
            }
        } catch (Exception e) {
            filename = "";
        }
 
        return filename;
    }
 
}