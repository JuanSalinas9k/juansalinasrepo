package com.fileutil;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;
import java.util.UUID;

/**
 * Class that allows to save to disk with a unique name
 */
public class FileSaver {
   
    private FileOutputStream fop = null;
    private File file;
    private byte[] content;
    private String extension;
    private String directory;
    private String dPath;

    public FileSaver(String directory,byte[] content, String extension)
    {
        this.content = content;
        this.extension = extension;
        this.directory = directory;
    }

    public FileSaver(String dPath)
    {
        this.dPath = dPath;
    }

    /**
     * Saves the binary info to disk and returns the path
     * 
     */ 
    public String saveToDisk()
    {
        String filename;
        String path;
        //String directory = System.getenv("SERVER_FILE_PATH");
        long millis = System.currentTimeMillis();
        String datetime = new Date().toGMTString();
        String uuid = UUID.randomUUID().toString();
        datetime = datetime.replace(" ", "");
        datetime = datetime.replace(":", "");
        filename = uuid + "_" + datetime + "_" + millis;
        path = directory + filename + extension;

        try{
            file = new File(path);
            fop = new FileOutputStream(file);

            if(!file.exists())
            {
                file.createNewFile();
            }
            fop.write(content);
            fop.flush();
            fop.close();
        }
        catch (IOException e) 
        {
            e.printStackTrace();
            path = null;
        } 
        finally 
        {
            try 
            {
                if (fop != null) 
                {
                    fop.close();
                }
            } 
            catch (IOException e) 
            {
                e.printStackTrace();
                path = null;
			}
        }
        return path;
    }

    public Boolean annihilate()
    {
        Boolean result = true;

        file = new File(dPath);
        
        try
        {
            result = file.delete();
        }
        catch (SecurityException e) 
        {
            e.printStackTrace();
            result = false;
        } 
        
        return result;
    }
}