/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rps;
import java.util.Scanner;
/**
 *
 * @author mrcaf
 */
public class RPS {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Scanner scan = new Scanner(System.in);
        int userAns = 0;
        int pcAns = 0;
        boolean keepPlaying = true;
        String items[] = {"Rock","Paper","Scissors"};
        
        boolean userWon = false;
        boolean tie = false;
        
        do
        {
            tie = false;
            userWon = false;
            System.out.println("-----RPS 2K18 v0.001 by JerryBeans9K-----");
            System.out.println("1.Rock\t2.Paper\t3.Scissors");
            userAns = scan.nextInt();
            pcAns = (int)Math.ceil(Math.random() * 3);
            if(userAns == 3 && pcAns == 1)
                userWon = false;
            else if(userAns == 1 && pcAns == 3)
                userWon = true;
            else if(userAns > pcAns)
                userWon = true;
            else if(userAns < pcAns)
                userWon = false;
            else if(userAns == pcAns)
                tie = true;
            
            System.out.println("Seleccionaste: " + items[userAns-1]);
            System.out.println("El oponente selecciono: " + items[pcAns-1]);
            if(tie)
               System.out.println("---EMPATE---"); 
            else
            {
                if(userWon)
                    System.out.println("---GANASTE---");
                else
                    System.out.println("---PERDISTE---");
            }
            
            System.out.println("Quieres seguir jugando? 1.Si\tOtro numero.No");
            keepPlaying = (scan.nextInt() == 1)?true:false;
            System.out.println("--------------------------------------------");
        }while(keepPlaying);
    }
}
